import axios from "axios";
import React, { useEffect, useState } from "react";
import { getAllCameras } from "./api/CamerasApi";
import "./App.css";
import CameraList from "./components/CameraList";
import Map from "./components/Map";
import { Camera, SortedCameras } from "./types/Camera";

axios.defaults.baseURL = "http://localhost:8080";

const App = () => {
  const [cameras, setCameras] = useState<Camera[]>();
  const [sortedCameras, setSortedCameras] = useState<SortedCameras>();

  useEffect(() => {
    const init = async () => {
      const data = await getAllCameras();
      setCameras(data);
      setSortedCameras(sortCameras(data));
    };
    init();
  }, []);

  const sortCameras = (cameras: Camera[]): SortedCameras => {
    const sortedCameras: SortedCameras = {
      byThree: [],
      byFive: [],
      byThreeAndFive: [],
      notByThreeAndFive: [],
    };
    for (const camera of cameras) {
      const number = camera.cameraName.match("[0-9]+");
      if (Number(number) % 3 === 0) sortedCameras.byThree.push(camera);
      if (Number(number) % 5 === 0) sortedCameras.byFive.push(camera);
      if (Number(number) % 3 === 0 && Number(number) % 5 === 0)
        sortedCameras.byThreeAndFive.push(camera);
      if (Number(number) % 3 > 0 && Number(number) % 5 > 0)
        sortedCameras.notByThreeAndFive.push(camera);
    }
    return sortedCameras;
  };

  return (
    <div>
      <h1>Security cameras Utrecht</h1>
      <Map cameras={cameras} />
      <div id="source">
        source:
        <a href="https://data.overheid.nl/dataset/camera-s">
          https://data.overheid.nl/dataset/camera-s
        </a>
      </div>
      <main>
        <table id="cameraTableContainer">
          <tbody>
            <tr>
              <CameraList cameras={sortedCameras?.byThree} title="Camera 3" />
              <CameraList cameras={sortedCameras?.byFive} title="Camera 5" />
              <CameraList
                cameras={sortedCameras?.byThreeAndFive}
                title="Cameras 3 &amp; 5"
              />
              <CameraList
                cameras={sortedCameras?.notByThreeAndFive}
                title="Cameras overig"
              />
            </tr>
          </tbody>
        </table>
      </main>
    </div>
  );
};

export default App;
