import React from "react";
import { Camera } from "../types/Camera";

const CameraListItem = ({ camera }: { camera: Camera }) => {
  return (
    <tr>
      <td>{camera.cameraName}</td>
      <td>{camera.latitude}</td>
      <td>{camera.longitude}</td>
    </tr>
  );
};

export default CameraListItem;
