import React from "react";
import { MapContainer, TileLayer, Marker, Popup } from "react-leaflet";
import { Camera } from "../types/Camera";

const Map = ({ cameras }: { cameras: Camera[] }) => {
  return (
    <MapContainer id="map" center={[52.093421, 5.118278]} zoom={13}>
      <TileLayer
        attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
      />
      {cameras?.map((camera) => (
        <Marker
          key={camera.id}
          position={[camera?.latitude, camera?.longitude]}
        >
          <Popup maxWidth={2000}>
            <div className="flex flex-col text-xl gap-3">
              <span>{`Latitude: ${camera?.cameraName}`}</span>
              <span>{`Latitude: ${camera?.latitude}`}</span>
              <span>{`Longitude: ${camera?.longitude}`}</span>
              <img
                className="min-w-[300px] aspect-square"
                src={`https://maps.googleapis.com/maps/api/streetview?size=400x400&location=${camera.latitude},${camera.longitude}&fov=80&heading=70&pitch=0&key=${process.env.REACT_APP_API_KEY}`}
                alt={camera.cameraName}
              />
            </div>
          </Popup>
        </Marker>
      ))}
    </MapContainer>
  );
};
export default Map;
