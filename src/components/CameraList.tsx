import React from "react";
import { Camera } from "../types/Camera";
import CameraListItem from "./CameraListItem";

const CameraList = ({
  cameras,
  title,
}: {
  cameras: Camera[];
  title: string;
}) => {
  return (
    <td>
      <table id="column3">
        <thead>
          <tr>
            <th colSpan={4}>{title}</th>
          </tr>
          <tr>
            <th>Number</th>
            <th>Name</th>
            <th>Latitude</th>
            <th>Longitude</th>
          </tr>
        </thead>
        <tbody>
          {cameras?.map((camera) => (
            <CameraListItem key={camera?.id} camera={camera} />
          ))}
        </tbody>
      </table>
    </td>
  );
};

export default CameraList;
