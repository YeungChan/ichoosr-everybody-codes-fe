import { Camera } from "./../types/Camera";
import axios from "axios";

export const getAllCameras = async (): Promise<Camera[]> => {
  const response = await axios.get("/api/cameras");
  return response.data;
};
