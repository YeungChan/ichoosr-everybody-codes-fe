export interface Camera {
  id: number;
  cameraName: string;
  latitude: number;
  longitude: number;
}

export interface SortedCameras{
    byThree:Camera[];
    byFive:Camera[];
    byThreeAndFive:Camera[];
    notByThreeAndFive:Camera[];
};

